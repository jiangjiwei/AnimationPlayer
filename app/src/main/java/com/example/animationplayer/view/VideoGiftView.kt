package com.example.animationplayer.view

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import android.widget.RelativeLayout
import androidx.lifecycle.LifecycleOwner
import com.example.animationplayer.R
import com.example.animationplayer.tool.Debugs
import com.example.animationplayer.tool.ExoPlayerImpl
import com.example.animationplayer.tool.JsonUtil
import com.ss.ugc.android.alpha_player.IMonitor
import com.ss.ugc.android.alpha_player.IPlayerAction
import com.ss.ugc.android.alpha_player.controller.IPlayerController
import com.ss.ugc.android.alpha_player.controller.PlayerController
import com.ss.ugc.android.alpha_player.model.AlphaVideoViewType
import com.ss.ugc.android.alpha_player.model.Configuration
import com.ss.ugc.android.alpha_player.model.DataSource

class VideoGiftView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    companion object {
        const val TAG = "VideoGiftView"
    }

    private val mVideoContainer: RelativeLayout
    private var mPlayerController: IPlayerController? = null

    init {
        LayoutInflater.from(context).inflate(getResourceLayout(), this)
        mVideoContainer = findViewById(R.id.byteAlphaVideoView)
    }

    private fun getResourceLayout(): Int {
        return R.layout.view_video_gift
    }

    fun initPlayerController(context: Context, owner: LifecycleOwner, playerAction: IPlayerAction, monitor: IMonitor) {
        val configuration = Configuration(context, owner)
        //  GLTextureView supports custom display layer, but GLSurfaceView has better performance, and the GLSurfaceView is default.
        configuration.alphaVideoViewType = AlphaVideoViewType.GL_TEXTURE_VIEW
        //  You can implement your IMediaPlayer, here we use ExoPlayerImpl that implemented by ExoPlayer, and
        //  we support DefaultSystemPlayer as default player.
        mPlayerController = PlayerController.get(configuration, ExoPlayerImpl(context))
        mPlayerController?.let {
            it.setPlayerAction(playerAction)
            it.setMonitor(monitor)
        }
    }

    fun startVideoGift(filePath: String) {
        Debugs.e(TAG, "resource path $filePath")
        if (TextUtils.isEmpty(filePath)) {
            return
        }
        val configModel = JsonUtil.parseConfigModel(filePath)
        Debugs.d(TAG, "config -> ${configModel.toString()}")
        configModel?.let {
            val dataSource = DataSource()
                .setBaseDir(if (filePath.endsWith("/")) filePath else "$filePath/")
                .setPortraitPath(it.portraitItem!!.path!!, it.portraitItem!!.alignMode)
                .setLandscapePath(it.landscapeItem!!.path!!, it.landscapeItem!!.alignMode)
                .setLooping(false)
            Debugs.d(TAG, "animation file path ${dataSource.getPath(android.content.res.Configuration.ORIENTATION_PORTRAIT)}")
            startDataSource(dataSource)
        }
    }

    private fun startDataSource(dataSource: DataSource) {
        if (!dataSource.isValid()) {
            Debugs.e(TAG, "startDataSource: dataSource is invalid.")
        }
        mPlayerController?.start(dataSource)
    }

    fun attachView() {
        mPlayerController?.attachAlphaView(mVideoContainer)
        Debugs.d(TAG, "attach alphaVideoView")
    }

    fun detachView() {
        mPlayerController?.detachAlphaView(mVideoContainer)
        Debugs.d(TAG, "detach alphaVideoView")
    }

    fun releasePlayerController() {
        mPlayerController?.let {
            it.detachAlphaView(mVideoContainer)
            it.release()
        }
    }
}