package com.example.animationplayer.bean

import com.google.gson.annotations.SerializedName

class ConfigModel {
    @SerializedName("landscape")
    var landscapeItem: Item? = null

    @SerializedName("portrait")
    var portraitItem: Item? = null

    class Item {
        @SerializedName("path")
        var path: String? = null

        @SerializedName("align")
        var alignMode: Int = 0
        override fun toString(): String {
            return "Item(path=$path, alignMode=$alignMode)"
        }
    }

    override fun toString(): String {
        return "ConfigModel(landscapeItem=$landscapeItem, portraitItem=$portraitItem)"
    }
}