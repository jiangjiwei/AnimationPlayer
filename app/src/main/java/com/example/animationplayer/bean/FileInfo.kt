package com.example.animationplayer.bean

/**
 * @description
 * @author: Created jiangjiwei in 2021/7/15 2:56 下午
 */
data class FileInfo(
    val fileName: String = "",
    val md5: String = ""
)