package com.example.animationplayer.page

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.SeekBar.OnSeekBarChangeListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.animationplayer.R
import com.example.animationplayer.ribbon.RibbonConfig
import com.example.animationplayer.ribbon.RibbonDrawable
import com.example.animationplayer.ribbon.RibbonShape
import java.util.*

class RibbonActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "RibbonActivity"
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, RibbonActivity::class.java)
            context.startActivity(starter)
        }
    }

    private lateinit var ivPlayer: ImageView
    private lateinit var cbCircle: CheckBox
    private lateinit var cbRect: CheckBox
    private lateinit var cbRectLong: CheckBox
    private lateinit var cbTriangle: CheckBox
    private lateinit var cbStar: CheckBox
    private lateinit var sbVelocity: SeekBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ribbon)

        ivPlayer = findViewById(R.id.ivPlayer)
        cbCircle = findViewById(R.id.cbCircle)
        cbRect = findViewById(R.id.cbRect)
        cbRectLong = findViewById(R.id.cbRectLong)
        cbStar = findViewById(R.id.cbStart)
        cbTriangle = findViewById(R.id.cbTriangle)
        sbVelocity = findViewById(R.id.sbVelocity)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            sbVelocity.min = 1
        }
        sbVelocity.max = 10
        sbVelocity.progress = 5
        sbVelocity.setOnSeekBarChangeListener(object : OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                Log.d(TAG, "速度进度为 $progress")
                if (progress < 1) {
                    sbVelocity.progress = 1
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    fun onStartClick(view: View) {
        val types: MutableList<Int> = ArrayList()
        if (cbTriangle.isChecked) {
            types.add(RibbonShape.TRIANGLE)
        }
        if (cbStar.isChecked) {
            types.add(RibbonShape.STAR)
        }
        if (cbRect.isChecked) {
            types.add(RibbonShape.RECT)
        }
        if (cbRectLong.isChecked) {
            types.add(RibbonShape.RECT_LONG)
        }
        if (cbCircle.isChecked) {
            types.add(RibbonShape.CIRCLE)
        }
        if (types.isEmpty()) {
            Toast.makeText(this, "未选择图形", Toast.LENGTH_SHORT).show()
            return
        }
        val typeArray = IntArray(types.size)
        for (i in types.indices) {
            typeArray[i] = types[i]
        }

        val drawable = RibbonDrawable(this)
        val normalConfig = getNormalConfig(typeArray, sbVelocity.progress)
        drawable.setRibbonConfig(normalConfig)
        ivPlayer.setImageDrawable(drawable)

        drawable.setAnimationListener(object : RibbonDrawable.AnimationListener {
            override fun onStart() {
                Log.d(TAG, "onStart: Ribbon Drawable")
            }

            override fun onStopCreate() {
                Log.d(TAG, "onStopCreate: Ribbon Drawable")
            }

            override fun onStop() {
                Log.d(TAG, "onStop: Ribbon Drawable")
            }
        })

        cbCircle.postDelayed({
            drawable.start()
        }, 500)

    }

    fun pColor(c: String) = Color.parseColor(c)

    private fun getNormalConfig(shapeTypes: IntArray, velocity: Int): RibbonConfig {
        return RibbonConfig()
            .setColors(intArrayOf(pColor("#FFD53034"), pColor("#FFFFD83A"), pColor("#FFE073FF"), pColor("#FF3FDBEA")))
            .setShapeTypes(shapeTypes)
            .setVelocity(velocity)
            .setCreadCount(10)
            .setMinScaleX(1.5f)
            .setMaxScaleX(1.8f)
            .setMinScaleY(1.5f)
            .setMaxScaleY(1.8f)
            .setDuration(1500)
    }
}