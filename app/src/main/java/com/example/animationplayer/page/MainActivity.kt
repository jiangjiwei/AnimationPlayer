package com.example.animationplayer.page

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.animationplayer.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnAnimTencentVap.setOnClickListener {
            VapAnimationActivity.start(this)
        }

        binding.btnAnimByteAlpha.setOnClickListener {
            AlphaAnimationActivity.start(this)
        }

        binding.btnShapeAnimation.setOnClickListener {
            RibbonActivity.start(this)
        }
    }

}