package com.example.animationplayer.tool

import android.util.Log
import androidx.annotation.IntRange
import com.example.animationplayer.BuildConfig

/**
 * @description
 * @author: Created jiangjiwei
 */
object Debugs {
    fun stackTrace(self: Any? = null, @IntRange(from = 2) limit: Int = 3): String? {
        if (!BuildConfig.DEBUG) return ""
        val result = StringBuilder()
        var length = 0
        for ((i, it) in Thread.currentThread().stackTrace.withIndex()) {
            if (i < 4 || it.methodName.contains("\$default")) continue
            if (length > 0) result.append("<-")
            if (self == null || self.javaClass.name != it.className) {
                result.append(it.className).append(".")
            }
            result.append(it.methodName.replace("\$app_debug", "") + "(${it.lineNumber})")
            length++
            if (length >= limit) break
        }
        return result.toString()
    }

    fun dTrace(self: Any? = null, @IntRange(from = 2) limit: Int = 3, tag: String, content: String) {
        if (!BuildConfig.DEBUG) return
        Log.d(tag, content +" " + stackTrace(self, limit))
    }

    fun d(tag: String = "Debugs", content: String) {
        if (!BuildConfig.DEBUG) return
        Log.d(tag, content)
    }

    fun i(tag: String = "Debugs", content: String) {
        if (!BuildConfig.DEBUG) return
        Log.i(tag, content)
    }

    fun e(tag: String = "Debugs", content: String) {
        if (!BuildConfig.DEBUG) return
        Log.e(tag, content)
    }
}