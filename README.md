# AnimationPlayer

此 Demo 内含有两套视频动画的播放组件，分别是来自腾讯的 [Vap](https://github.com/Tencent/vap) 和来自字节跳动的 [AlphaPlayer](https://github.com/bytedance/AlphaPlayer)。

[Demo Apk 下载地址](http://monster-image-backup.oss-cn-shanghai.aliyuncs.com/picgo/report/report-vap-alpha-compare-app-debug.apk)

## Vap

添加一个 View 后，对 View 进行初始化，设置监听器、资源拉取监听后即可播放本地文件的动画。

### 工具资源

有图形化页面进行操作[VapTool](https://github.com/Tencent/vap/tree/master/tool)

## AlphaPlayer

需要自己封装 View，以及接入 Exo 播放器 Json 解析（Json 文件可省，在代码中动态配置），最终完成动画的播放，不可添加自定义资源及音频。

## 横向对比

|                  | 腾讯-Vap                           | 字节-AlphaPlayer                           |
| ---------------- | ---------------------------------- | ------------------------------------------ |
| 原理             | -                                  | -                                          |
| 适用平台         | 1. Android<br />2. iOS<br />3. Web | 1. Android<br />2. iOS                     |
| 透明动画         | -                                  | -                                          |
| 融合元素         | 支持多张图片、多个文本             | 无                                         |
| 支持声音         | 是                                 | 无                                         |
| 导出工具         | 提供二次封装工具                   | AE 可直接导出                              |
|                  |                                    |                                            |
| 平台维护         | 及时更新、issue 回复率高           | 更新较慢，最近更新 3 个月前                |
| 同一动画文件大小 | 930 KB                             | 1.2 MB                                     |
| 代码包体积       | 176 KB                             | 128 KB<br />另需引入 ExoPlayerCore 1M 左右 |
| 接入准备工作     | 无                                 | 实现 ExoPlay 播放控制器                    |
| 内存占用         | -                                  | -                                          |
| CPU 占用率       | -                                  | -                                          |

![全部内存占用对比](http://monster-image-backup.oss-cn-shanghai.aliyuncs.com/picgo/report/vap-alpha-all-memory-compare.png)

![图形内存占用对比](http://monster-image-backup.oss-cn-shanghai.aliyuncs.com/picgo/report/vap-alpha-graphics-compare.png)

1. mp4 文件中插入特定区域写入 Alpha 值，运行时利用 OpenGL 动态生成 ARGB 图形
2. 字节的 AlphaPlayer Android 端没有带有音频的测试文件，iOS 版本明确不能播放音频；
3. 图表数据为 10 次测试产生数据取平均值单位 MB，[图表源数据文件下载地址](http://monster-image-backup.oss-cn-shanghai.aliyuncs.com/picgo/report/Vap-Alpha-%E6%A8%AA%E5%90%91%E5%AF%B9%E6%AF%94.xlsx)。
4. CPU 占用率均相差无几
5. 峰值指页面从创建到展示整个过程中的内存占用峰值